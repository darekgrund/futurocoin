'use strict';

(function ($) {
    var config,
        chartsData,
        chart;

    config = {
        yobit: {
            api: 'http://api.blockchain24.co/fto/data'
        }
    };

    chartsData = {
        'yobit': {
            labels: [],
            datasets: [{
                label: 'Yobit price (USD)',
                fill: false,
                borderColor: '#0198D0',
                backgroundColor: '#0198D0',
                data: []
            }]
        }
    };

    chart = new Chart($('#market-performance-chart'), {
        type: 'line',
        data: chartsData.yobit,
        options: {
            responsive: true,
            maintainAspectRatio: false,

            tooltips: {
                mode: 'index',
                intersect: false
            },
            elements: {
                line: {
                    tension: 0.000001
                }
            },
            plugins: {
                filler: {
                    propagate: false
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false
                    }
                }]
            }
        }
    });


    var prepareYobitData = function(data) {
        var date;
        var items = data.map(function (item) {
            date = moment(item.updated_at).format('HH:mm');
            return {
                value: item.price,
                label: date,
                volume: item.vol
            }
        });
        return items;
    };


    var addChartValue = function(item) {
        chart.data.datasets[0].data.push(item.value);
        chart.data.labels.push(item.label);
    };


    $.ajax({
        type: 'GET',
        url: config.yobit.api,
        success: function (response) {
            var data = prepareYobitData(response.data);
            data.forEach(function (item) {
                addChartValue(item);
            });
            chart.update();

            var current = data.pop();
            $('[data-fto-price]').text('$' + Number(current.value).toFixed(2));
            $('[data-fto-volume]').text('$' + Number(current.volume / 1000).toFixed(2) + ' K');
        }
    });
}(jQuery));