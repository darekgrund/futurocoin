'use strict';
(function ($) {
    var $header = $('.js-header');
    $(document).ready(function () {
        // Smooth scroll
        $('a[href*=\\#].js-smooth-scroll-to').on('click', function (e) {
            var headerHeight = $header.outerHeight();
            var scrollTop = $(this.hash).offset().top;

            if (window.innerWidth >= 992) {
                scrollTop -= headerHeight;
            }

            scrollTop < 0 ? scrollTop = 0 : false;

            e.preventDefault();
            $('html, body').animate({
                scrollTop: scrollTop
            }, 500);
        });


        // Newsletter
        var $newsletterForm = $('.js-newsletter-form');
        $newsletterForm.parsley()
        .on('field:validated', function() {

        })
        .on('form:submit', function () {
            return false;
        });


        // News
        var $newsContainer = $('.js-news');
        $newsContainer.on('click', '.js-news--button-more', function (e) {
            var $buttonMore = $(this);
            $newsContainer.find('.js-news--more-container').slideDown(function () {
                $buttonMore.addClass('hidden');
            });
            e.preventDefault();
        });


        // Trailers
        var $trailerContainer = $('.trailer__container');
        $trailerContainer.slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 300,
            adaptiveHeight: true,
            arrows: false
        });

        var $trailerNav = $('.trailer__nav');
        $trailerNav.on('click', '.js-trailer--prev, .js-trailer--next', function (e) {
            console.log('kllklksdcsdc');
            if ($(this).hasClass('js-trailer--prev')) {
                $trailerContainer.slick('prev');
            } else if ($(this).hasClass('js-trailer--next')) {
                $trailerContainer.slick('next');
            }
            e.preventDefault();
        });


        // Animation on scroll
        AOS.init({
            duration: 300,
            easing: 'ease-in-sine',
            delay: 0,
            once: true,
            disable: window.innerWidth < 992
        });


        // particles.js
        particlesJS.load('particles-js', 'config/particlesjs-config.json');


        // Mobile menu
        var $mobileNavContainer = $('.js-mobile-nav--container');
        $mobileNavContainer.each(function () {
            var $mobileNav = $(this).find('.js-mobile-nav--nav');
            var $mobileNavSwitch = $(this).find('.js-mobile-nav--switch');
            var toggleNavigation = function () {
                $mobileNavSwitch.toggleClass('open');
                $mobileNav.slideToggle();
            };

            $mobileNavContainer.on('click', '.js-mobile-nav--switch', toggleNavigation);

            $mobileNavContainer.on('click', '.nav__link', function() {
                if ($mobileNavSwitch.hasClass('open')) {
                    toggleNavigation();
                }
            });
        });
    });

    $(window).scroll(function () {
        var scrollTop = $(this).scrollTop();
        if (scrollTop > 150) {
            $header.addClass('header--slim');
        } else if (scrollTop < 100) {
            $header.removeClass('header--slim');
        }
    });
    $(window).trigger('scroll');
})(jQuery);
