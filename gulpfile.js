'use strict';
var defaultLang = 'en'; // used to create ./build templates - when no language is specified

var config = {
    path: {
        src: {
            scss: [
                './resources/assets/styles/**/*.sass',
                './resources/assets/styles/**/*.scss'
            ]
        }
    },
    assetsVersion: Date.now()
};
var buildDir = './build';

var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename'),
    clip = require('gulp-clip-empty-files'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    nunjucksRender = require('gulp-nunjucks-render'),
    sassVariables = require('gulp-sass-variables'),
    debug = require('fancy-log'),
    mergeJson = require('gulp-merge-json'),
    data = require('gulp-data'),
    sequence = require('run-sequence'),
    del = require('del'),
    fs = require('fs'),
    path = require('path');


/**
 * Everything that ends in ./build directory is done here
 */

// Scripts
gulp.task('scripts', function() {
    return gulp.src('./resources/assets/scripts/*.js')
        .pipe(plumber())
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(buildDir + '/scripts'))
        .pipe(browserSync.stream({once: true}));
});


// Styles
gulp.task('styles', function() {
    return gulp.src(config.path.src.scss)
        .pipe(clip())
        .pipe(sassVariables({
            $assetsVersion: config.assetsVersion
        }))
        .pipe(sass({
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(buildDir + '/css/'))
        .pipe(reload({stream:true}));
});


// Copy files
gulp.task('copy', function() {
    return gulp.src([
        './resources/assets/vendor/**/*.*',
        './resources/assets/images/**/*.*',
        './resources/assets/fonts/**/*.*',
        './resources/assets/files/**/*.*',
        './resources/assets/config/**/*.*'
    ], {base: 'resources/assets'
    }).pipe(gulp.dest(buildDir));
});


// Delete build
gulp.task('clean:build', function () {
    return del(buildDir);
});


// Browser sync
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: buildDir,
            directory: true
        }
    });
});


// Compile nunjucks files
gulp.task('nunjucks', function() {
    return gulp.src('./resources/templates/pages/**/*.+(html|njk)')
        .pipe(plumber())
        .pipe(data(function () {
            //var fileContent = fs.readFileSync('./tmp/translations/'+ defaultLang +'.json', "utf8");
            return {
                //t: JSON.parse(fileContent),
                config: config
            }
        }))
        .pipe(nunjucksRender({
            path: ['./resources/templates']
        }))
        .pipe(gulp.dest(buildDir))
        .pipe(browserSync.stream({once: true}));
});


// Build project
gulp.task('build', function () {
    sequence(
        ['clean:build'],
        ['copy', 'scripts', 'styles'],
        'nunjucks'
    );
});


// Watch file changes
gulp.task('watch', function () {
    gulp.watch("./resources/assets/scripts/*.js", ['scripts']);
    gulp.watch(config.path.src.scss, ['styles']);
    gulp.watch("./resources/templates/**/*.+(html|njk)", ['nunjucks']);
});


// Default task
gulp.task('default', function () {
   sequence('build', 'browser-sync', 'watch');
});
